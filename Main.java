/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danilo.cadastraraluno;

import java.util.Scanner;
import java.util.ArrayList;

/**
 *
 * @author Danilo
 */
public class Main {
    
    public static void main(String[] args){
        int opc,quantnotas;
        String nome,cpf;
        Scanner teclado = new Scanner(System.in);
        Sala novaSala = new Sala();
        ArrayList<Sala> sal = new ArrayList<>();
        
        do{
           System.out.println("=====MENU=====");
           System.out.println("1- Inserir");
           System.out.println("2- Listar");           
           System.out.println("3- Situações");
           System.out.println("4- Sair");
           System.out.println("Opção: "); 
           opc = teclado.nextInt();
           teclado.nextLine(); 
           switch(opc){
                case 1:
                      System.out.println("\nDigite o Nome do aluno:");
                      nome = teclado.nextLine();
                      System.out.println("Digite o Cpf do aluno:");
                      cpf = teclado.nextLine();
                      System.out.println("Quantas notas deseja inserir a esse aluno:");
                      quantnotas = teclado.nextInt();
                      float notas[] = new float[quantnotas];
                      for (int i = 0; i < quantnotas; i++) {
                          System.out.println("Nota "+(i+1)+ ":");
                          notas[i] = teclado.nextFloat();
                    }
                      Aluno novoAluno = new Aluno (nome,cpf);
                      for (int i = 0; i < quantnotas; i++) {
                        novoAluno.InsereNota(notas[i]);
                    }
                     novaSala.insereAluno(novoAluno);
                   break;
                case 2:
                    novaSala.listartodos();
                   break;
                case 3:
                     novaSala.verSituacoes(); 
                   break;
                case 4:
                    System.out.println("Saindo..!");
                   break;
                default:
                    System.out.println("Opção Invalida!!");
                    break;
            }
        }while(opc!=4);
    }
    
}

